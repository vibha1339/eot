package com.eot;

public class ScoreBoard {
    int playerScore1;
    int playerScore2;

    ScoreBoard(int ps1 , int ps2){
        this.playerScore1 = ps1;
        this.playerScore2 = ps2;
    }

    public void updateScore(int ps1 , int ps2){
        this.playerScore1 = ps1;
        this.playerScore2 = ps2;
    }

    public ScoreBoard getScore(){
        return this;
    }

}
