package com.eot;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ScoreBoardTest {

    public ScoreBoard scoreBoard;

    @Before
    public void setup(){
        this.scoreBoard = new ScoreBoard(0,0);
    }

    @Test
    public void should_update_scoreboard(){
        this.scoreBoard.updateScore(2,2);
        boolean scoreValidate = this.scoreBoard.playerScore1 ==2 && this.scoreBoard.playerScore2 ==2 ;

        Assert.assertEquals(true , scoreValidate);
    }

}
