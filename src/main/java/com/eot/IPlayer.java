package com.eot;

public interface IPlayer {

    String getMove();
    int getScore();
    void updateScore(int score);
}
