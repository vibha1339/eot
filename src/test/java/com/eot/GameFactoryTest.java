package com.eot;

import org.junit.Test;
import org.mockito.internal.matchers.InstanceOf;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class GameFactoryTest {


    @Test
    public void create_game_should_always_return_game(){

        GameFactory factory = new GameFactory();
        assertNotNull(factory.createGame(PlayerType.CONSOLE,PlayerType.CONSOLE));
    }

    @Test

    public void create_game_should_accept_player_type_and_return_game_instance(){

        GameFactory factory = new GameFactory();
        Game game  = factory.createGame(PlayerType.CONSOLE,PlayerType.DUMMYCHEAT);

        assertTrue(game.player1 instanceof ConsolePlayer);
        assertTrue(game.player2 instanceof DummyPlayer);
    }

}
