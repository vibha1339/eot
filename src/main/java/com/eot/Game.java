package com.eot;

import java.io.PrintStream;

public class Game {

    private Machine machine;
    private PrintStream output;
    public IPlayer player1;
    public IPlayer player2;

    Game(Machine machine, PrintStream output , IPlayer player1 , IPlayer player2) {

        this.machine = machine;
        this.output = output;
        this.player1 = player1;
        this.player2 = player2;
    }

    public void startGame(int rounds) {

        for(int i=0;i<rounds; i++) {

            String firstPlayerMove = player1.getMove();
            String secondPlayerMove = player2.getMove();

            ScoreBoard scoreBoard = machine.processGame(firstPlayerMove, secondPlayerMove);
            updatePlayerScores(scoreBoard);
            output.println(player1.getScore() + " " + player2.getScore());
        }
    }

    public void updatePlayerScores(ScoreBoard scoreBoard){
        player1.updateScore(scoreBoard.playerScore1);
        player2.updateScore(scoreBoard.playerScore2);
    }
}
