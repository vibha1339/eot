package com.eot;

public interface GameInputReader {

    String read();

}
