package com.eot;

import java.util.Scanner;

public class ConsoleInputReader implements GameInputReader {

    private Scanner scanner;

    ConsoleInputReader(){
        scanner = new Scanner(System.in);
    }

    @Override
    public String read() {
        return scanner.nextLine();
    }
}
