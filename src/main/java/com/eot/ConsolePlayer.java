package com.eot;

public class ConsolePlayer implements IPlayer{


    private String name;
    private int score;
    private String move;
    private GameInputReader inputReader;// ch or co

    public ConsolePlayer(GameInputReader inputReader, String name , int score) {
        this.inputReader = inputReader;
        this.name = name;
        this.score  = score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void updateScore(int score) {
        this.score += score;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public String getMove() {
        return inputReader.read();
    }
}
