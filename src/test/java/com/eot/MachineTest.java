package com.eot;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MachineTest {

    public Machine machine;

    @Before
    public void setup(){
        this.machine=new Machine();
    }

    @Test
    public void should_return_2_2_when_values_co_co(){
        ScoreBoard scoreBoard = new Machine().processGame("co","co");
        boolean res = scoreBoard.playerScore1 == 2 && scoreBoard.playerScore2 ==2;
        Assert.assertEquals(true , res);
    }

    @Test
    public void should_return_neg1_3_when_values_co_ch(){
        ScoreBoard scoreBoard = new Machine().processGame("co","ch");
        boolean res = scoreBoard.playerScore1 == -1 && scoreBoard.playerScore2 ==3;
        Assert.assertEquals(true , res);
    }

    @Test
    public void should_return_3_neg1_when_values_ch_co(){
        ScoreBoard scoreBoard = new Machine().processGame("ch","co");
        boolean res = scoreBoard.playerScore1 == 3 && scoreBoard.playerScore2 == -1;
        Assert.assertEquals(true , res);
    }

    @Test
    public void should_return_0_0_when_values_ch_ch(){
        ScoreBoard scoreBoard = new Machine().processGame("ch","ch");
        boolean res = scoreBoard.playerScore1 == 0 && scoreBoard.playerScore2 ==0;
        Assert.assertEquals(true , res);
    }
}
