package com.eot;

public class Machine {

    public ScoreBoard scoreBoard;

    public Machine(){
        this.scoreBoard = new ScoreBoard(0,0);
    }

    public ScoreBoard processGame(String pm1 , String pm2){
        if(pm1.equals("co") && pm2.equals("co"))
        {
            scoreBoard.updateScore(2,2);
        } else if(pm1.equals("ch") && pm2.equals("co"))
        {
            scoreBoard.updateScore(3,-1);
        } else if(pm1.equals("co") && pm2.equals("ch"))
        {
            scoreBoard.updateScore(-1,3);
        }else{
            scoreBoard.updateScore(0,0);
        }
        return  scoreBoard;
    }

}
