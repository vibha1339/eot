package com.eot;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DummyPlayerTest {

    @Test
    public void should_return_only_specified_move(){
        DummyPlayer player = new DummyPlayer("ch");
        assertEquals("ch",player.getMove());

        player = new DummyPlayer("co");
        assertEquals("co",player.getMove());
    }

}
