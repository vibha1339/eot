package com.eot;

import java.io.PrintStream;
import java.util.HashMap;

public class GameFactory {

    private static HashMap<PlayerType, IPlayer> players = new HashMap();

    static{

        players.put(PlayerType.CONSOLE, new ConsolePlayer(new ConsoleInputReader(), "playerName", 0 ));
        players.put(PlayerType.DUMMYCHEAT, new DummyPlayer( "ch"));
        players.put(PlayerType.DUMMYCOOPERATE,new DummyPlayer( "co"));

    }

    public static Game createGame(PlayerType p1Type, PlayerType p2Type) {
        return new Game(new Machine(),new PrintStream(System.out),players.get(p1Type),players.get(p2Type));
    }
}
