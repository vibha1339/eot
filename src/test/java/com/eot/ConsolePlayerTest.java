package com.eot;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.validateMockitoUsage;

public class ConsolePlayerTest {

    public  ConsolePlayer player;
    private GameInputReader  scanner;

    @Before
    public void setup(){

        scanner = mock(GameInputReader.class);
        this.player= new ConsolePlayer(scanner,"Player1",0);
    }

    @Test
    public void should_intantiate_player(){
        assertEquals("Player1", this.player.getName());
    }

    @Test
    public void checking_player_score(){
        this.player.updateScore(10);
        assertEquals(10, this.player.getScore());
    }

    @Test
    public void should_return_input_from_scanner(){
        Mockito.when(scanner.read()).thenReturn("ch").thenReturn("co");
        assertEquals("ch", this.player.getMove());
        assertEquals("co",this.player.getMove());
    }
}
