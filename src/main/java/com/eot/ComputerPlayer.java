package com.eot;

class DummyPlayer  implements  IPlayer{

    private String move;
    private int score;
    public DummyPlayer(String move){
        this.move = move;
    }

    @Override
    public String getMove() {
        return move;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void updateScore(int score) {
        this.score += score;

    }
}
