package com.eot;

import org.junit.Before;
import org.junit.Test;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class GameTest {

    public Game game;
    private GameInputReader scanner;
    private PrintStream output;

    @Before
    public void setup(){
        scanner = mock(GameInputReader.class);
        ConsolePlayer player1 = new ConsolePlayer(scanner,"Player1" , 0);
        ConsolePlayer player2 = new ConsolePlayer(scanner,"Player2", 0);

        Machine machine = new Machine();

        output = mock(PrintStream.class);
        this.game=new Game( machine, output, player1,player2 );
    }

    @Test
    public void shouldPlayGameForOneRound(){

        when(scanner.read()).thenReturn("co").thenReturn("ch");
        game.startGame(1);
        verify(output).println("-1 3");

    }

    @Test
    public void shouldReturn3_neg1OnPassingChCO(){

        when(scanner.read()).thenReturn("ch").thenReturn("co");
        game.startGame(1);
        verify(output).println("3 -1");

    }

    @Test
    public void shouldReturn5_neg1PassingCoCh(){

        when(scanner.read()).thenReturn("co").thenReturn("co").thenReturn("ch").thenReturn("co");

        game.startGame(2);
        verify(output).println("5 1");

    }

    @Test
    public void shouldReturn(){

        when(scanner.read()).thenReturn("co").thenReturn("co").thenReturn("ch").thenReturn("co").thenReturn("co").thenReturn("co");
        game.startGame(3);
        // game.updatePlayerScores()

        verify(output).println("7 3");

    }
}
